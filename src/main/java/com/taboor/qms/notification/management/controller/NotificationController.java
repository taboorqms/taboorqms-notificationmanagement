package com.taboor.qms.notification.management.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.taboor.qms.core.utils.RSocketPayload;
import com.taboor.qms.notification.management.service.NotificationService;

@RestController
@RequestMapping("/notification")
@CrossOrigin(origins = "*")
public class NotificationController {

	private static final Logger logger = LoggerFactory.getLogger(NotificationController.class);

	private NotificationService notificationService;

	public NotificationController(NotificationService notificationService) {
		super();
		this.notificationService = notificationService;
	}

	@PostMapping("/handle")
	public @ResponseBody Boolean handleNotifications(@RequestBody RSocketPayload payload) throws Exception, Throwable {
		logger.info("Calling Get Handle Notification - API");
		notificationService.decodePayload(payload);
		return true;
	}

}
