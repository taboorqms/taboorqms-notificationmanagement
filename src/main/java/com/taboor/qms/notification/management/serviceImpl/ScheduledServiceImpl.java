package com.taboor.qms.notification.management.serviceImpl;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.taboor.qms.core.exception.TaboorQMSServiceException;
import com.taboor.qms.core.model.Invoice;
import com.taboor.qms.core.utils.InvoiceStatus;
import com.taboor.qms.core.utils.InvoiceType;
import com.taboor.qms.notification.management.repositories.InvoiceRepository;
import com.taboor.qms.notification.management.service.ScheduledService;

@Service
public class ScheduledServiceImpl implements ScheduledService {

	@PersistenceContext
	EntityManager em;

	@Autowired
	InvoiceRepository invoiceRepository;

	@Override
	@Transactional
	public void updateInvoiceStatus() throws TaboorQMSServiceException, Exception {
		final String updateInvoiceStatusQuery = "update tab_invoice set invoice_status = '::OVERDUE::' where '::currentDate::' > date(due_date) and invoice_status IN ('::UNPAID::','::DRAFT::')";
		Query query = em.createNativeQuery(updateInvoiceStatusQuery.replace("::OVERDUE::", InvoiceStatus.OVERDUE.name())
				.replace("::currentDate::", LocalDate.now().toString())
				.replace("::UNPAID::", InvoiceStatus.UNPAID.name()).replace("::DRAFT::", InvoiceStatus.DRAFT.name()));
		query.executeUpdate();
	}

	@Override
	public void chargeInvoices() throws TaboorQMSServiceException, Exception {
		List<Invoice> invoices = invoiceRepository.findByTypeAndStatusAndDueDate(InvoiceType.AUTO, InvoiceStatus.UNPAID,
				LocalDate.now());
		System.out.println("Invoices: " + invoices.size());
	}

	@Override
	public void updateBranchTicketsPerDay() throws TaboorQMSServiceException, Exception {
		Query query = em.createNativeQuery(
				"select count(branch_id), branch_id from tab_ticket where date(created_time) = '::currentDate::' group by branch_id"
						.replace("::currentDate::", LocalDate.now().toString()));
		List<Object[]> resultSet = query.getResultList();

		for (Object[] item : resultSet) {
			query = em.createNativeQuery(
					"update tab_branch set average_ticket_per_day = (average_ticket_per_day * average_ticket_per_day_count + ::newCount::)/(average_ticket_per_day_count + 1), average_ticket_per_day_count = average_ticket_per_day_count + 1 where branch_id = ::branch_id::"
							.replace("::newCount::", item[0].toString()).replace("::branch_id::", item[1].toString()));
			query.executeUpdate();
		}

	}

}
