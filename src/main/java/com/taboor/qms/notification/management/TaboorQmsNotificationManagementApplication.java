package com.taboor.qms.notification.management;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.taboor.qms.core.exception.TaboorQMSServiceException;
import com.taboor.qms.notification.management.service.ScheduledService;

@SpringBootApplication
@ComponentScan({ "com.taboor.qms.notification.management", "com.taboor.qms.core" })
@EnableJpaRepositories(basePackages = { "com.taboor.qms.notification.management.repositories" })
@EntityScan("com.taboor.qms.core.model")
@EnableAsync
@EnableScheduling
public class TaboorQmsNotificationManagementApplication implements CommandLineRunner {

	private ScheduledService scheduledService;

	public TaboorQmsNotificationManagementApplication(ScheduledService scheduledService) {
		super();
		this.scheduledService = scheduledService;
	}

	public static void main(String[] args) {
		SpringApplication.run(TaboorQmsNotificationManagementApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

		FirebaseOptions options = new FirebaseOptions.Builder()
				.setCredentials(GoogleCredentials.fromStream(getClass().getClassLoader()
						.getResourceAsStream("taboor-firebase-adminsdk-0p6y8-b25b763338.json")))
				.setDatabaseUrl("https://taboor.firebaseio.com").build();

		FirebaseApp.initializeApp(options);
	}

	// At 00:00:00am every day - 0 0 0 * * ?
	@Scheduled(cron = "0 0 0 * * ?")
	void scheduledFunction() throws TaboorQMSServiceException, Exception {
		System.out.println("Scheduled CRON Job Started");
		scheduledService.chargeInvoices();
		scheduledService.updateInvoiceStatus();
		scheduledService.updateBranchTicketsPerDay();
	}

}
