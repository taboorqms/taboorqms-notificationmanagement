package com.taboor.qms.notification.management.serviceImpl;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.taboor.qms.core.exception.TaboorQMSServiceException;
import com.taboor.qms.core.model.Notification;
import com.taboor.qms.core.utils.NotificationStatus;
import com.taboor.qms.core.utils.NotificationType;
import com.taboor.qms.core.utils.RSocketNotificationSubject;
import com.taboor.qms.core.utils.RSocketPayload;
import com.taboor.qms.notification.management.service.NotificationService;

@Service
public class NotificationServiceImpl implements NotificationService {

	@Autowired
	AsyncNotificationSender asyncNotificationSender;

	@PersistenceContext
	EntityManager em;

	@Override
	public void decodePayload(RSocketPayload payload) throws TaboorQMSServiceException, Exception {

		final String taboorEmpQuery = "select s.device_token, u.email, u.phone_number, u.user_id, uc.in_app_notification_allowed, uc.email_allowed, uc.sms_allowed from tab_taboor_employee te inner join tab_user u on u.user_id = te.user_id inner join tab_user_notification_type unt on unt.user_id = te.user_id inner join tab_user_configuration uc on uc.user_id = te.user_id left outer join tab_session s on s.user_id = te.user_id where unt.::typeKey:: = true and s.logout_time is null";
		final String serviceCenterEmpQuery = "select s.device_token, u.email, u.phone_number, u.user_id, uc.in_app_notification_allowed, uc.email_allowed, uc.sms_allowed from tab_service_center_employee sce inner join tab_user u on u.user_id = sce.user_id inner join tab_user_notification_type unt on unt.user_id = sce.user_id inner join tab_user_configuration uc on uc.user_id = sce.user_id left outer join tab_session s on s.user_id = sce.user_id where sce.service_center_id = ::centerId:: and unt.::typeKey:: = true and s.logout_time is null";
		final String customerRatingEmpQuery = "select s.device_token, u.email, u.phone_number, u.user_id, uc.in_app_notification_allowed, uc.email_allowed, uc.sms_allowed from tab_service_center_employee sce inner join tab_employee_branch_mapper ebm on ebm.employee_id = sce.employee_id inner join tab_user u on u.user_id = sce.user_id inner join tab_user_notification_type unt on unt.user_id = sce.user_id inner join tab_user_configuration uc on uc.user_id = sce.user_id left outer join tab_session s on s.user_id = sce.user_id where ebm.branch_id = ::branchId:: and unt.::typeKey:: = true and s.logout_time is null";
		final String supportTicketUpdateQuery = "select s.device_token, u.email, u.phone_number, u.user_id, uc.in_app_notification_allowed, uc.email_allowed, uc.sms_allowed from tab_user u inner join tab_user_notification_type unt on unt.user_id = u.user_id inner join tab_user_configuration uc on uc.user_id = u.user_id left outer join tab_session s on s.user_id = u.user_id where u.user_id = ::userId:: and unt.::typeKey:: = true and s.logout_time is null";

		Query query = null;
		Notification notification = new Notification();

		if (payload.getNotificationSubject().equals(RSocketNotificationSubject.SUPPORT_TICKET_UPDATE)) {
			query = em.createNativeQuery(supportTicketUpdateQuery.replace("::typeKey::", "update_support_ticket")
					.replace("::userId::", String.valueOf(payload.getRecipientId())));
			notification.setCreatedAt(OffsetDateTime.now());
			notification.setNotificationStatus(NotificationStatus.Sent);
			notification.setReadStatus(false);
			notification.setTitle("Support Ticket is Updated");
			notification.setMessage("Your Support Ticket No. '" + payload.getInformation().get(0) + "' is updated. "
					+ payload.getInformation().get(1));
			notification.setMetaData(null);
		} else if (payload.getNotificationSubject().equals(RSocketNotificationSubject.CUSTOMER_RATING)) {
			query = em.createNativeQuery(customerRatingEmpQuery.replace("::typeKey::", payload.getInformation().get(0))
					.replace("::branchId::", String.valueOf(payload.getRecipientId())));
			notification.setCreatedAt(OffsetDateTime.now());
			notification.setNotificationStatus(NotificationStatus.Sent);
			notification.setReadStatus(false);
			notification.setTitle("Customer added new rating");
			notification.setMessage("Customer added rating of '" + payload.getInformation().get(1).toUpperCase()
					+ "' against serving of a ticket.");
			notification.setMetaData(null);
		} else if (payload.getNotificationSubject().equals(RSocketNotificationSubject.NEW_SERVICE_CENTER)) {
			query = em.createNativeQuery(taboorEmpQuery.replace("::typeKey::", "new_or_update_service_center"));
			notification.setCreatedAt(OffsetDateTime.now());
			notification.setNotificationStatus(NotificationStatus.Sent);
			notification.setReadStatus(false);
			notification.setTitle("New Service Center Registered");
			notification
					.setMessage("New Service Center '" + payload.getInformation().get(0) + "' Registered in TaboorQMS");
			notification.setMetaData(null);
		} else if (payload.getNotificationSubject().equals(RSocketNotificationSubject.UPDATE_SERVICE_CENTER)) {
			query = em.createNativeQuery(taboorEmpQuery.replace("::typeKey::", "new_or_update_service_center"));
			notification.setCreatedAt(OffsetDateTime.now());
			notification.setNotificationStatus(NotificationStatus.Sent);
			notification.setReadStatus(false);
			notification.setTitle("Service Center Updated");
			notification.setMessage("Service Center '" + payload.getInformation().get(0) + "' updated in TaboorQMS");
			notification.setMetaData(null);
		} else if (payload.getNotificationSubject().equals(RSocketNotificationSubject.NEW_TABOOR_USER)) {
			query = em.createNativeQuery(taboorEmpQuery.replace("::typeKey::", "new_or_update_user"));
			notification.setCreatedAt(OffsetDateTime.now());
			notification.setNotificationStatus(NotificationStatus.Sent);
			notification.setReadStatus(false);
			notification.setTitle("New Taboor User Registered");
			notification
					.setMessage("New Taboor User '" + payload.getInformation().get(0) + "' Registered in TaboorQMS");
			notification.setMetaData(null);
		} else if (payload.getNotificationSubject().equals(RSocketNotificationSubject.UPDATE_TABOOR_USER)) {
			query = em.createNativeQuery(taboorEmpQuery.replace("::typeKey::", "new_or_update_user"));
			notification.setCreatedAt(OffsetDateTime.now());
			notification.setNotificationStatus(NotificationStatus.Sent);
			notification.setReadStatus(false);
			notification.setTitle("Taboor User Updated");
			notification.setMessage("Taboor User '" + payload.getInformation().get(0) + "' updated in TaboorQMS");
			notification.setMetaData(null);
		} else if (payload.getNotificationSubject().equals(RSocketNotificationSubject.NEW_BRANCH)) {
			query = em.createNativeQuery(serviceCenterEmpQuery.replace("::typeKey::", "new_or_update_branch")
					.replace("::centerId::", String.valueOf(payload.getRecipientId())));
			notification.setCreatedAt(OffsetDateTime.now());
			notification.setNotificationStatus(NotificationStatus.Sent);
			notification.setReadStatus(false);
			notification.setTitle("New Branch is added");
			notification.setMessage("New Branch '" + payload.getInformation().get(0) + "' is added in Service Center");
			notification.setMetaData(null);
		} else if (payload.getNotificationSubject().equals(RSocketNotificationSubject.UPDATE_BRANCH)) {
			query = em.createNativeQuery(serviceCenterEmpQuery.replace("::typeKey::", "new_or_update_branch")
					.replace("::centerId::", String.valueOf(payload.getRecipientId())));
			notification.setCreatedAt(OffsetDateTime.now());
			notification.setNotificationStatus(NotificationStatus.Sent);
			notification.setReadStatus(false);
			notification.setTitle("Branch is Updated");
			notification.setMessage("Branch '" + payload.getInformation().get(0) + "' is updated in Service Center");
			notification.setMetaData(null);
		} else if (payload.getNotificationSubject().equals(RSocketNotificationSubject.NEW_AGENT)) {
			query = em.createNativeQuery(serviceCenterEmpQuery.replace("::typeKey::", "new_or_update_agent")
					.replace("::centerId::", String.valueOf(payload.getRecipientId())));
			notification.setCreatedAt(OffsetDateTime.now());
			notification.setNotificationStatus(NotificationStatus.Sent);
			notification.setReadStatus(false);
			notification.setTitle("New Agent is added");
			notification.setMessage("New Agent '" + payload.getInformation().get(0) + "' is added in Service Center");
			notification.setMetaData(null);
		} else if (payload.getNotificationSubject().equals(RSocketNotificationSubject.UPDATE_AGENT)) {
			query = em.createNativeQuery(serviceCenterEmpQuery.replace("::typeKey::", "new_or_update_agent")
					.replace("::centerId::", String.valueOf(payload.getRecipientId())));
			notification.setCreatedAt(OffsetDateTime.now());
			notification.setNotificationStatus(NotificationStatus.Sent);
			notification.setReadStatus(false);
			notification.setTitle("Agent is Updated");
			notification.setMessage("Agent '" + payload.getInformation().get(0) + "' is updated in Service Center");
			notification.setMetaData(null);
		} else if (payload.getNotificationSubject().equals(RSocketNotificationSubject.NEW_SERVICECENTER_USER)) {
			query = em.createNativeQuery(serviceCenterEmpQuery.replace("::typeKey::", "new_or_update_user")
					.replace("::centerId::", String.valueOf(payload.getRecipientId())));
			notification.setCreatedAt(OffsetDateTime.now());
			notification.setNotificationStatus(NotificationStatus.Sent);
			notification.setReadStatus(false);
			notification.setTitle("New User is added");
			notification.setMessage("New User '" + payload.getInformation().get(0) + "' is added in Service Center");
			notification.setMetaData(null);
		} else if (payload.getNotificationSubject().equals(RSocketNotificationSubject.UPDATE_SERVICECENTER_USER)) {
			query = em.createNativeQuery(serviceCenterEmpQuery.replace("::typeKey::", "new_or_update_user")
					.replace("::centerId::", String.valueOf(payload.getRecipientId())));
			notification.setCreatedAt(OffsetDateTime.now());
			notification.setNotificationStatus(NotificationStatus.Sent);
			notification.setReadStatus(false);
			notification.setTitle("User is Updated");
			notification.setMessage("User '" + payload.getInformation().get(0) + "' is updated in Service Center");
			notification.setMetaData(null);
		}
		List<Object[]> results = query.getResultList();

		if (!results.isEmpty()) {

			List<String> deviceTokenList = new ArrayList<String>();
			List<String> emailList = new ArrayList<String>();
			List<String> phoneNumberList = new ArrayList<String>();
			List<Long> userIdList = new ArrayList<Long>();

			for (Object[] item : results) {
				if (item[4] == null)
					item[4] = false;
				if (item[5] == null)
					item[5] = false;
				if (item[6] == null)
					item[6] = false;
				if (!deviceTokenList.contains(item[0]) && item[0] != null
						&& Boolean.valueOf(item[4].toString()) == true)
					deviceTokenList.add(String.valueOf(item[0]));
				if (!emailList.contains(item[1]) && Boolean.valueOf(item[5].toString()) == true)
					emailList.add(String.valueOf(item[1]));
				if (!phoneNumberList.contains(item[2]) && Boolean.valueOf(item[6].toString()) == true)
					phoneNumberList.add(String.valueOf(item[2]));
				if (!userIdList.contains(Long.valueOf(String.valueOf(item[3]))))
					userIdList.add(Long.valueOf(String.valueOf(item[3])));
			}

			System.out.println("User Count: " + userIdList.size());

			if (payload.getTypes().contains(NotificationType.IN_APP)) {
				notification.setNotificationType(NotificationType.IN_APP.getType());
				asyncNotificationSender.pushInAppNotifications(deviceTokenList, userIdList,
						new Notification(notification));
			}
			if (payload.getTypes().contains(NotificationType.EMAIL)) {
				notification.setNotificationType(NotificationType.EMAIL.getType());
				asyncNotificationSender.sendEmails(emailList, userIdList, new Notification(notification));
			}
			if (payload.getTypes().contains(NotificationType.SMS)) {
				asyncNotificationSender.sendSMS(phoneNumberList);
			}
		}

	}
}
