package com.taboor.qms.notification.management.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.taboor.qms.core.model.Notification;

@Repository
@Transactional
public interface NotificationRepository extends JpaRepository<Notification, Long> {

}
