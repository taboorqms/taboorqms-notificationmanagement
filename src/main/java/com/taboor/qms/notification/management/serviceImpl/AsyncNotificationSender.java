package com.taboor.qms.notification.management.serviceImpl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.google.firebase.messaging.FirebaseMessagingException;
import com.taboor.qms.core.exception.TaboorQMSServiceException;
import com.taboor.qms.core.model.Notification;
import com.taboor.qms.core.model.User;
import com.taboor.qms.core.utils.NotificationSender;
import com.taboor.qms.notification.management.repositories.NotificationRepository;

@Service
public class AsyncNotificationSender {

	@Autowired
	NotificationRepository notificationRepository;

	private static final org.slf4j.Logger logger = LoggerFactory.getLogger(AsyncNotificationSender.class);

	@Async
	public void pushInAppNotifications(List<String> deviceTokenList, List<Long> userIdList, Notification notification)
			throws TaboorQMSServiceException {

		List<Notification> notificationList = new ArrayList<Notification>();
		for (Long userId : userIdList) {
			User user = new User();
			user.setUserId(userId);
			notification.setUser(user);
			notificationList.add(new Notification(notification));
		}

		System.out.println("FB Count: " + deviceTokenList.size());
		for (String token : deviceTokenList)
			try {
				logger.info(token);
				NotificationSender.sendPushNotification(notification.getTitle(), notification.getMessage(), token);
			} catch (FirebaseMessagingException | TaboorQMSServiceException e) {
				e.printStackTrace();
			}

		System.out.println("INAPP Count: " + notificationList.size());
		notificationRepository.saveAll(notificationList);
	}

	@Async
	public void sendEmails(List<String> emailList, List<Long> userIdList, Notification notification) {

		List<Notification> notificationList = new ArrayList<Notification>();
		for (Long userId : userIdList) {
			User user = new User();
			user.setUserId(userId);
			notification.setUser(user);
			notificationList.add(new Notification(notification));
		}

		for (String emailAddress : emailList)
			System.out.println(emailAddress);

		System.out.println("Email Count: " + notificationList.size());

		notificationRepository.saveAll(notificationList);
	}

	@Async
	public void sendSMS(List<String> phoneNumberList) {

		for (String phoneNumber : phoneNumberList)
			System.out.println(phoneNumber);
	}

}
